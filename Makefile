#Wlasny plik makefile

FLAGS = -c -g -iquote inc -Wall -pedantic
FINAL = -Wall -pedantic

output: main.o LZesp.o
	g++ ${FINAL} main.o LZesp.o -o output
	rm *.o


main.o: src/main.cpp inc/LZesp.hh inc/uklad.hh
	g++ ${FLAGS} src/main.cpp

LZesp.o: src/LZesp.cpp inc/LZesp.hh
	g++ ${FLAGS} src/LZesp.cpp


clean:
	rm *.o output