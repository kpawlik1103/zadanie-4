#include "LZesp.hh"

#include <iostream>
#include <iomanip>
#include <cmath>
#include <unistd.h>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

bool LZespolona::operator = (int data)
{
    //  niejawne rzutowanie int -> double
    double value = data;

    this->re = value;
    this->im = value;

    return true;
}

istream & operator >> (istream & str_we, LZespolona & data)
{
    char nawL, nawP, i;
    double newRe, newIm;

    str_we >> nawL >> newRe >> newIm >> i >> nawP;

    if(nawL == '(')
    {
        if(i == 'i')
        {
            if(nawP == ')')
            {
                data.re = newRe;
                data.im = newIm;

                return str_we;
            }
        }
    }

    str_we.ignore(1024, '\n');
    str_we.clear();

    cout << "Blad! To nie jest liczba zespolona." << endl;

    return str_we;
}

ostream & operator << (ostream & str_wy, LZespolona & data)
{
    /*

        slaba opcja w sumie

    str_wy << '(';
    str_wy.width(5);
    str_wy << setprecision(4) << data.re;

    str_wy.width(5);
    str_wy << showpos;
    str_wy << setprecision(4) << data.im;
    str_wy << "i)" << noshowpos;
    */

    str_wy << '(';
    if(data.re < 0) str_wy << '-';
    else str_wy << '+';

    str_wy.width(5);
    str_wy << fixed << setprecision(2) << abs(data.re) << ' ';

    if(data.im < 0) str_wy << '-';
    else str_wy << '+';

    str_wy.width(5);
    str_wy << fixed << setprecision(2) << abs(data.im) << ' ';
    str_wy << 'i' << ')';

    return str_wy;
}

LZespolona operator + (LZespolona skl_1, LZespolona skl_2)
{
    LZespolona tmp;

    tmp.re = skl_1.re + skl_2.re;
    tmp.im = skl_1.im + skl_2.im;

    return tmp;
}

LZespolona operator - (LZespolona skl_1, LZespolona skl_2)
{
    LZespolona tmp;

    tmp.re = skl_1.re - skl_2.re;
    tmp.im = skl_1.im - skl_2.im;

    return tmp;
}

LZespolona operator * (LZespolona skl_1, LZespolona skl_2)
{
    LZespolona tmp;

    tmp.re = (skl_1.re * skl_2.re) - (skl_1.im * skl_2.im);
    tmp.im = (skl_1.re * skl_2.im) + (skl_1.im * skl_2.re);

    return tmp;
}

LZespolona operator * (int skl_1, LZespolona skl_2)
{
    LZespolona tmp;

    double stc = static_cast<double>(skl_1);

    tmp.re = skl_2.re * stc;
    tmp.im = skl_2.im * stc;

    return tmp;
}

LZespolona operator / (LZespolona skl_1, LZespolona skl_2)
{
    /** UWAGA!!! Funkcja sprawdza, czy nie wystepuje dzielenie przez zero.  */
    /** Jesli wystepuje dzielenie przez zero, funkcja wyswietla odpowiedni komunikat
        na wyjsciu diagnostycznym oraz zwraca wartosc pierwszej liczby!!!   */

    if(Modul(skl_2) == 0)
    {
        /*  Funkcja wykonuje sie w momencie wykrycia proby dzielenia przez zero.    */

        cout << "*  -   -   -   -   -   -   -   -   -   -   -   -   *" << endl;
        cout << "|  Blad! Dzielenie przez zero.                     |" << endl;
        cout << "|  Nastapi zwrocenie wartosci pierwszej skladowej. |" << endl;
        cout << "*  -   -   -   -   -   -   -   -   -   -   -   -   *" << endl;

        return skl_1;
    }

    LZespolona tmp;

    tmp = (skl_1 * Sprzezenie(skl_2)) / Modul(skl_2);

    return tmp;
}

LZespolona operator % (LZespolona skl_1, LZespolona skl_2)
{
    LZespolona tmp;

    //  Koniecznosc rzutowania double na int
    int new_re_1, new_im_1;
    int new_re_2, new_im_2;
    int value_1, value_2;

    new_re_1 = static_cast<int>(skl_1.re);
    new_re_2 = static_cast<int>(skl_2.re);
    new_im_1 = static_cast<int>(skl_1.im);
    new_im_2 = static_cast<int>(skl_2.im);

    value_1 = new_re_1 % new_re_2;
    value_2 = new_im_1 % new_im_2;

    //  Konwersja do double i zapis danych
    tmp.re = static_cast<double>(value_1);
    tmp.im = static_cast<double>(value_2);

    return tmp;
}

LZespolona operator / (LZespolona skl_1, double skl_modul)
{
    LZespolona tmp;

    tmp.re = skl_1.re / pow(skl_modul, 2);
    tmp.im = skl_1.im / pow(skl_modul, 2);

    return tmp;
}

bool operator == (LZespolona skl_1, LZespolona skl_2)
{
    if(skl_1.re == skl_2.re && skl_1.im == skl_2.im)    return true;
    else                                                return false;
}

LZespolona Sprzezenie(LZespolona data)
{
    data.im = data.im * (-1);

    return data;
}

double Modul(LZespolona data)
{
    double tmp;

    tmp = sqrt(pow(data.re, 2) + pow(data.im, 2));

    return tmp;
}

LZespolona Oblicz(LZespolona skl_1, LZespolona skl_2, char znak)
{
    LZespolona tmp;

    switch(znak)
    {
    case '+':
        tmp = skl_1 + skl_2;
        break;

    case '-':
        tmp = skl_1 - skl_2;
        break;

    case '*':
        tmp = skl_1 * skl_2;
        break;

    case '/':
        tmp = skl_1 / skl_2;
        break;
    //    Nowy case - %
    case '%':
        tmp = skl_1 % skl_2;
        break;
    }

    return tmp;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

//  Wsztstko jest w porzadku