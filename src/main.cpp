#include "LZesp.hh"
#include "uklad.hh"

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
    char type;
    cin >> type;

    cout << fixed << setprecision(2);

    if(type == 'z')
    {
        //  przypadek dla liczb zespolonych
        uklad<LZespolona, ROZMIAR> dataZ;
        wektor<LZespolona, ROZMIAR> valueZ;
        cin >> dataZ;
        cout << dataZ << endl;

        //  rozwiazanie ukladu
        valueZ = dataZ.calculate();
        cout << "Rozwiazanie x = (x1, x2, x3) :" << endl;
        cout << valueZ << endl;

        //  wektor bledu
        valueZ = dataZ.errorvalue();
        cout << "Wektor bledu: Ax - b = " << valueZ << endl;

        return 0;
    }

    if(type == 'r')
    {
        //  przypadek dla liczb rzeczywistych
        uklad<double, ROZMIAR> dataR;
        wektor<double, ROZMIAR> valueR;
        cin >> dataR;
        cout << dataR << endl;

        //  rozwiazanie ukladu
        valueR = dataR.calculate();
        cout << "Rozwiazanie x = (x1, x2, x3) :" << endl;
        cout << valueR << endl;

        //  wektor bledu
        valueR = dataR.errorvalue();
        cout << "Wektor bledu: Ax - b = " << valueR << endl;

        return 0;
    }

    //  blad wyboru
    cout << "Blad! Podano zly typ danych." << endl;

    return 1;
}