#ifndef WEKTOR_HH
#define WEKTOR_HH

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#include "rozmiar.hh"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
class wektor
{
    private:

        type coordinate[ROZMIAR];

    public:

        wektor(){}

        type operator [] (unsigned int) const;

        type & operator [] (unsigned int);

        void load();

        void show();

        wektor operator + (const wektor<type, size>) const;

        wektor operator - (const wektor<type, size>) const;

        type operator * (const wektor<type, size>) const;
};

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
istream & operator >> (istream &, wektor<type, size> &);

template<typename type, int size>
ostream & operator << (ostream &, wektor<type, size> &);

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
type wektor<type, size>::operator [] (unsigned int index) const
{
    return coordinate[index];
}

template<typename type, int size>
type & wektor<type, size>::operator [] (unsigned int index)
{
    return coordinate[index];
}


template<typename type, int size>
void wektor<type, size>::load()
{
    for(int i = 0; i < size; i++)
    {
        cin >> coordinate[i];
    }
}

template<typename type, int size>
void wektor<type, size>::show()
{
    for(int i = 0; i < size; i++)
    {
        cout << '\t' << coordinate[i];
    }
}

template<typename type, int size>
wektor<type, size> wektor<type, size>::operator + (const wektor<type, size> data) const
{
    wektor tmp;

    for(int i = 0; i < size; i++)
    {
        tmp[i] = coordinate[i] + data.coordinate[i];
    }

    return tmp;
}

template<typename type, int size>
wektor<type, size> wektor<type, size>::operator - (const wektor<type, size> data) const
{
    wektor tmp;

    for(int i = 0; i < size; i++)
    {
        tmp[i] = coordinate[i] - data.coordinate[i];
    }

    return tmp;
}

template<typename type, int size>
type wektor<type, size>::operator * (const wektor<type, size> data) const
{
    type tmp;

    for(int i = 0; i < size; i++)
    {
        tmp = tmp + coordinate[i] * data.coordinate[i];
    }

    return tmp;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
istream & operator >> (istream & str, wektor<type, size> & data)
{
    data.load();

    return str;
}

template<typename type, int size>
ostream & operator << (ostream & str, wektor<type, size> & data)
{
    data.show();

    return str;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#endif