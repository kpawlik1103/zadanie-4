#ifndef UKLAD_HH
#define UKLAD_HH

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#include "macierz.hh"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
class uklad
{
    private:

        macierz<type, size> matrix;
        wektor<type, size> freeExp;
        wektor<type, size> error;

    public:

        uklad(){}

        void load();

        void show();

        wektor<type, size> calculate();

        wektor<type, size> errorvalue();
};

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
istream & operator >> (istream &, uklad<type, size> &);

template<typename type, int size>
ostream & operator << (ostream &, uklad<type, size> &);

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
void uklad<type, size>::load()
{
    cin >> matrix;
    cin >> freeExp;
}

template<typename type, int size>
void uklad<type, size>::show()
{
    cout << "Macierz A^T :" << endl;
    cout << matrix << endl;

    cout << "Wektor wyrazow wolnych :" << endl;
    cout << freeExp << endl;
}

template<typename type, int size>
wektor<type, size> uklad<type, size>::calculate()
{
    macierz<type, size> Tmp;
    wektor<type, size> tmp;

    for(int i = 0; i < size; i++)
    {
        Tmp = matrix;
        Tmp.changeColumn(freeExp, i);
        tmp[i] = Tmp.determiant();
    }

    return tmp;
}

template<typename type, int size>
wektor<type, size> uklad<type, size>::errorvalue()
{
    return error;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
istream & operator >> (istream & str, uklad<type, size> & data)
{
    data.load();
    return str;
}

template<typename type, int size>
ostream & operator << (ostream & str, uklad<type, size> & data)
{
    data.show();
    return str;
}


/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#endif