#ifndef LZESP_HH
#define LZESP_HH

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#include <iostream>
#include <cmath>
#include <unistd.h>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

//  Struktura liczby zespolonej

struct LZespolona
{
    //  Atrybuty
    double re;
    double im;

    bool operator = (int);
};

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

//  Prototypy funkcji dla liczby zespolonej

istream & operator >> (istream & str_we, LZespolona & data);

ostream & operator << (ostream & str_wy, LZespolona & data);

LZespolona operator + (LZespolona skl_1, LZespolona skl_2);

LZespolona operator - (LZespolona skl_1, LZespolona skl_2);

LZespolona operator * (LZespolona skl_1, LZespolona skl_2);

LZespolona operator * (int skl_1, LZespolona skl_2);

LZespolona operator / (LZespolona skl_1, LZespolona skl_2);

LZespolona operator % (LZespolona skl_1, LZespolona skl_2);

LZespolona operator / (LZespolona skl_1, double skl_modul);

bool operator == (LZespolona skl_1, LZespolona skl_2);

LZespolona Sprzezenie(LZespolona data);

double Modul(LZespolona data);

LZespolona Oblicz(LZespolona skl_1, LZespolona skl_2, char znak);

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

//  Wsztstko jest w porzadku

#endif