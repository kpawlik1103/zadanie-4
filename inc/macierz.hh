#ifndef MACIERZ_HH
#define MACIERZ_HH

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#include "wektor.hh"
#include "wyznacznik.hh"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
class macierz
{
    private:

        wektor<type, size> column[size];

    public:

        macierz(){}

        type operator () (unsigned int, unsigned int) const;

        type & operator () (unsigned int, unsigned int);

        void load();

        void show();

        macierz transpose() const;

        type determiant() const;

        wektor<type, size> operator * (wektor<type, size>);

        void changeColumn(wektor<type, size>, unsigned int);
};

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
istream & operator >> (istream &, macierz<type, size> &);

template<typename type, int size>
ostream & operator << (ostream &, macierz<type, size> &);

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
type macierz<type, size>::operator () (unsigned int unit, unsigned int list) const
{
    return column[unit][list];
}

template<typename type, int size>
type & macierz<type, size>::operator () (unsigned int unit, unsigned int list)
{
    return column[unit][list];
}

template<typename type, int size>
void macierz<type, size>::load()
{
    for(int i = 0; i < size; i++)
    {
        cin >> column[i];
    }
}

template<typename type, int size>
void macierz<type, size>::show()
{
    for(int i = 0; i < size; i++)
    {
        cout << column[i] << endl;
    }
}

template<typename type, int size>
macierz<type, size> macierz<type, size>::transpose() const
{
    macierz<type, size> tmp;

    for(int i = 0; i < size; i++)
    {
        for(int j = 0; j < size; j++)
        {
            tmp(i, j) = column[j][i];
        }
    }

    return tmp;
}

template<typename type, int size>
type macierz<type, size>::determiant() const
{
        int n = size;
    int w = 0;
    int *K;
    type **A;
    type end;

    A = new type * [n];
    for(int i = 0; i < n; i++)
    {
        A[i] = new type [n];
        for(int j = 0; j < n; j++)  {A[i][j] = column[i][j];}
    }

    K = new int [n];
    for(int i = 0; i < n; i ++) {K[i] = i;}

    end = determ(n, w, K, A);

    delete [] K;
    for(int i = 0; i < n; i++)  {delete [] A[i];}
    delete [] A;

    return end;
}

template<typename type, int size>
wektor<type, size> macierz<type, size>::operator * (wektor<type, size> data)
{
    type tmp;
    wektor<type, size> TMP;

    for(int i = 0; i < size; i++)
    {
        tmp = column[i] * data;
        TMP[i] = tmp;
    }

    return TMP;
}

template<typename type, int size>
void macierz<type,size>::changeColumn(wektor<type, size> data, unsigned int index)
{
    column[index] = data;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

template<typename type, int size>
istream & operator >> (istream & str, macierz<type, size> & data)
{
    data.load();

    return str;
}

template<typename type, int size>
ostream & operator << (ostream & str, macierz<type, size> & data)
{
    data.show();

    return str;
}

/*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

#endif